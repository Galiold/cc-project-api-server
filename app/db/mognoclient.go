package db

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var RestaurantDatabase *mongo.Database
var RestaurantCollection *mongo.Collection
var OrderCollection *mongo.Collection
var UserCollection *mongo.Collection

//var OrderCollection *mongo.Collection

func Init() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(
		"mongodb://restaurantapp_mongodb:27017/",
	))
	if err != nil {
		panic(err)
	}

	RestaurantDatabase = client.Database("cc-project")
	RestaurantCollection = RestaurantDatabase.Collection("restaurants")
	OrderCollection = RestaurantDatabase.Collection("orders")
	UserCollection = RestaurantDatabase.Collection("users")

}
