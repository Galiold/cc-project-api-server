package controllers

import (
	"api-server/app/db"
	"api-server/app/models"
	"github.com/revel/revel"
	"go.mongodb.org/mongo-driver/bson"
)

type App struct {
	*revel.Controller
}

type Response struct {
	*revel.Controller
	Status    string   `json:"status"`
	Message   string   `json:"message,omitempty"`
	ResultObj bson.M   `json:"result_obj,omitempty"`
	ResultArr []bson.M `json:"result_arr,omitempty"`
}

func init() {
	db.Init()
	models.AddDefaultUsers()
	models.CreateRestaurantUniqueIndex()
}

func (c App) Index() revel.Result {
	test := "TEST"
	return c.Render(test)
}
