package controllers

import (
	"api-server/app/db"
	"api-server/app/models"
	"context"
	"encoding/json"
	"github.com/revel/revel"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func (c App) GetRestaurants() revel.Result {
	var restaurants []bson.M
	var res Response
	var failRes Response
	failRes.Status = "failed"
	failRes.Message = "Failed to fetch restaurants"

	cur, err := db.RestaurantCollection.Find(
		context.TODO(),
		bson.D{{}},
		options.Find().SetProjection(bson.M{"_id": 0, "menu": 0}))

	if err != nil {
		log.Println(err)
		c.Response.Status = 500
		return c.RenderJSON(failRes)
	}

	for cur.Next(context.TODO()) {
		var elem bson.M
		if err := cur.Decode(&elem); err != nil {
			log.Println(err)
			c.Response.Status = 500
			return c.RenderJSON(failRes)
		}
		restaurants = append(restaurants, elem)
	}

	if err := cur.Err(); err != nil {
		log.Println(err)
		c.Response.Status = 500
		return c.RenderJSON(failRes)
	}

	if err := cur.Close(context.TODO()); err != nil {
		log.Println(err)
		c.Response.Status = 500
		return c.RenderJSON(failRes)
	}

	res.Status = "ok"
	res.ResultArr = restaurants
	return c.RenderJSON(res)
}

func (c App) CreateRestaurant() revel.Result {
	var res Response
	var failRes Response
	res.Status = "ok"
	failRes.Status = "failed"

	var restaurant models.Restaurant
	if err := json.Unmarshal(c.Params.JSON, &restaurant); err != nil {
		c.Response.SetStatus(400)
		failRes.Message = "Error reading restaurant data from request body"
		return c.RenderJSON(failRes)
	}
	if inRes, err := db.RestaurantCollection.InsertOne(context.TODO(), restaurant); err != nil {
		log.Println(err)
		c.Response.SetStatus(400)
		failRes.Message = "A restaurant with the same name exists"
		return c.RenderJSON(failRes)
	} else {
		res.ResultObj = bson.M{"inserted_id": inRes.InsertedID}
	}
	return c.RenderJSON(res)
}

func (c App) UpdateRestaurant() revel.Result {
	var res Response
	var failRes Response
	res.Status = "ok"
	failRes.Status = "failed"
	restaurantName := c.Params.Get("res_name")
	var restaurant models.Restaurant

	log.Println(restaurantName)

	if err := db.RestaurantCollection.FindOne(
		context.TODO(),
		bson.M{"name": restaurantName},
	).Decode(&restaurant); err != nil {
		log.Println(err)
		c.Response.SetStatus(404)
		failRes.Message = "Can not find the requested restaurant"
		return c.RenderJSON(failRes)
	}

	log.Println(restaurant)

	if err := json.Unmarshal(c.Params.JSON, &restaurant); err != nil {
		c.Response.SetStatus(400)
		failRes.Message = "Error reading restaurant data from request body"
		return c.RenderJSON(failRes)
	}

	log.Println(restaurant)

	if _, err := db.RestaurantCollection.UpdateOne(
		context.TODO(), bson.M{"name": restaurantName}, bson.M{"$set": restaurant},
	); err != nil {
		log.Println(err)
		c.Response.SetStatus(500)
		failRes.Message = "An error occurred while updating the restaurant."
		return c.RenderJSON(failRes)
	} else {
		res.Message = "Restaurant updated successfully."
	}
	return c.RenderJSON(res)
}

func (c App) GetRestaurantMenu() revel.Result {
	var restaurantMenu bson.M
	var res Response
	var failRes Response
	failRes.Status = "failed"
	failRes.Message = "Failed to find the given restaurant"
	restaurantName := c.Params.Get("res_name")

	if err := db.RestaurantCollection.FindOne(
		context.TODO(),
		bson.M{"name": restaurantName},
		options.FindOne().SetProjection(bson.M{"_id": 0, "menu": 1})).Decode(&restaurantMenu); err != nil {
		log.Println(err)
		c.Response.SetStatus(404)
		return c.RenderJSON(failRes)
	}

	res.Status = "ok"
	res.ResultObj = restaurantMenu
	return c.RenderJSON(res)
}
