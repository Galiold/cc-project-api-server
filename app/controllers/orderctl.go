package controllers

import (
	"api-server/app/db"
	"api-server/app/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/revel/revel"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (c App) CreateOrder() revel.Result {
	var res Response
	var failRes Response
	failRes.Status = "failed"
	failRes.Message = "failed to create order"
	restaurantName := c.Params.Get("res_name")
	var data models.CreateOrd
	if err := json.Unmarshal(c.Params.JSON, &data); err != nil {
		c.Response.SetStatus(400)
		failRes.Message = "failed to parse order request body"
		return c.RenderJSON(failRes)
	}
	fmt.Println(data)

	restaurant := models.Restaurant{}
	err := db.RestaurantCollection.FindOne(
		context.TODO(),
		bson.M{"name": restaurantName}).Decode(&restaurant)
	if err != nil{
		c.Response.SetStatus(404)
		failRes.Message = "failed to find Restaurant"
		return c.RenderJSON(failRes)
	}
	var notFound bool
	for _, userFood := range data.Foods{
		notFound = true
		if !notFound {
			c.Response.SetStatus(403)
			failRes.Message = userFood.Name+" not in menu"
			return c.RenderJSON(failRes)
		}
		for _, resFood := range restaurant.Menu{
			if userFood.Name == resFood.Name && userFood.Size == resFood.Size{
				if userFood.Count > resFood.Count {
					c.Response.SetStatus(403)
					failRes.Message = userFood.Name+" is already finished"
					return c.RenderJSON(failRes)
				} else {
					notFound = false
				}
			}
		}
	}

	user := models.User{}
	err = db.UserCollection.FindOne(
		context.TODO(),
		bson.M{"$and": []bson.M{
			{"username": data.Username},
			{"usertype": models.Customer},
		}}).Decode(&user)
	if err != nil {
		fmt.Println(err)
		c.Response.SetStatus(403)
		failRes.Message = "you need to register"
		return c.RenderJSON(failRes)
	}

	var order = models.Order{
		User: user.ID,
		Restaurant: restaurant.ID,
		Orders: data.Foods,
		Status: models.Queued,
	}

	_, err = db.OrderCollection.InsertOne(
		context.TODO(), order)
	if err != nil {
		c.Response.SetStatus(500)
		failRes.Message = "can't create order"
		return c.RenderJSON(failRes)
	}

	res.Status = "ok"
	return c.RenderJSON(res)
}

func (c App) GetRestaurantOrders() revel.Result {
	var res Response
	var failRes Response
	res.Status = "ok"
	res.Message = "order status successfully changed"
	failRes.Status = "failed"
	restaurantID, err := primitive.ObjectIDFromHex(c.Params.Get("res_id"))

	ordersCursor ,err := db.OrderCollection.Find(
		context.TODO(),
		bson.M{"restaurant": restaurantID})

	if err != nil {
		fmt.Println(err)
		failRes.Message = "no orders for this restaurant"
		c.Response.SetStatus(404)
		return c.RenderJSON(failRes)
	}

	var orders []bson.M
	if err = ordersCursor.All(context.TODO(), &orders); err != nil {
		fmt.Println(err)
		failRes.Message = "sth went wrong"
		c.Response.SetStatus(500)
		return c.RenderJSON(failRes)
	}
	return c.RenderJSON(orders)
}

func (c App) ChangeStatusOrder() revel.Result {
	var res Response
	var failRes Response
	res.Status = "ok"
	failRes.Status = "failed"
	orderID, err := primitive.ObjectIDFromHex(c.Params.Get("order_id"))
	var restaurantName = c.Params.Get("res_name")
	var data models.ChangeStatus
	if err := json.Unmarshal(c.Params.JSON, &data); err != nil {
		c.Response.SetStatus(400)
		failRes.Message = "failed to parse change status request body"
		return c.RenderJSON(failRes)
	}

	var order models.Order
	err = db.OrderCollection.FindOne(
		context.TODO(),
		bson.M{"_id": orderID}).Decode(&order)
	if err != nil {
		fmt.Println(err)
		c.Response.SetStatus(404)
		failRes.Message = "failed to find order"
		return c.RenderJSON(failRes)
	}
	var restaurant models.Restaurant
	err = db.RestaurantCollection.FindOne(
		context.TODO(),
		bson.M{"name": restaurantName}).Decode(&restaurant)
	if err != nil {
		fmt.Println(err)
		c.Response.SetStatus(404)
		failRes.Message = "failed to find restaurant"
		return c.RenderJSON(failRes)
	}
	switch data.Action {
	case string(models.Pending):
		order.Status = models.Pending
		for _, orderFood := range order.Orders{
			for ind := range restaurant.Menu{
				if orderFood.Name == restaurant.Menu[ind].Name &&  orderFood.Size == restaurant.Menu[ind].Size{
					restaurant.Menu[ind].Count = restaurant.Menu[ind].Count - orderFood.Count
					fmt.Println(restaurant.Menu[ind])
				}
			}
		}

		fmt.Println(restaurant.Menu)
		_, err = db.RestaurantCollection.UpdateOne(
			context.TODO(),
			bson.M{"name": restaurantName},
			bson.M{"$push" : bson.M{"menu" : bson.M{"$each": restaurant.Menu, "$slice":-len(restaurant.Menu)}}})
		if err != nil {
			fmt.Println(err)
		}
		_, err = db.OrderCollection.UpdateOne(
			context.TODO(),
			bson.M{"_id": orderID},bson.M{
				"$set" : bson.M{"status": order.Status},
			})
	case string(models.Rejected):
		_, err = db.OrderCollection.UpdateOne(
			context.TODO(),
			bson.M{"_id": orderID},bson.M{
				"$set" : bson.M{"status": models.Rejected},
			})
		if err != nil {
			c.Response.SetStatus(404)
			failRes.Message = "failed to find order"
			return c.RenderJSON(failRes)
		}
	case string(models.Received):
		_, err = db.OrderCollection.UpdateOne(
			context.TODO(),
			bson.M{"_id": orderID},bson.M{
				"$set" : bson.M{"status": models.Received},
			})
		if err != nil {
			c.Response.SetStatus(404)
			failRes.Message = "failed to find order"
			return c.RenderJSON(failRes)
		}

	default:
		c.Response.SetStatus(400)
		failRes.Message = "invalid action"
		return c.RenderJSON(failRes)
	}

	return c.RenderJSON(res)
}
func (c App) GetUserOrders() revel.Result {
	var failRes Response
	userID, err := primitive.ObjectIDFromHex(c.Params.Get("user_id"))

	ordersCursor, err := db.OrderCollection.Find(
		context.TODO(),
		bson.M{"user": userID})

	if err != nil {
		fmt.Println(err)
		failRes.Message = "no orders for this restaurant"
		c.Response.SetStatus(404)
		return c.RenderJSON(failRes)
	}

	var orders []bson.M
	if err = ordersCursor.All(context.TODO(), &orders); err != nil {
		fmt.Println(err)
		failRes.Message = "sth went wrong"
		c.Response.SetStatus(500)
		return c.RenderJSON(failRes)
	}
	return c.RenderJSON(orders)
}
