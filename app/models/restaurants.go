package models

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type foodSize string

const (
	Small  foodSize = "small"
	Medium foodSize = "medium"
	Big    foodSize = "big"
)

type Food struct {
	Name        string   `json:"name,omitempty"`
	Price       float32  `json:"price,omitempty"`
	Size        foodSize `json:"size,omitempty"`
	Count       int32    `json:"count,omitempty"`
	Description string   `json:"description,omitempty"`
}

type Restaurant struct {
	ID		primitive.ObjectID	`bson:"_id,omitempty"`
	Owner   primitive.ObjectID `json:"owner,omitempty"`
	Name    string             `json:"name,omitempty"`
	Address string             `json:"address,omitempty"`
	Phone   int64              `json:"phone,omitempty"`
	Menu    []Food             `json:"menu,omitempty"`
	IsOpen  bool               `json:"isopen,omitempty"`
}

func (userType foodSize) IsValid() error {
	switch userType {
	case Small, Medium, Big:
		return nil
	}
	return errors.New("invalid food size")
}