package models

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type userType string

const (
	Customer userType = "customer"
	Owner    userType = "owner"
)

type User struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	Username string             `json:"username,omitempty"`
	Passwd   string             `json:"passwd,omitempty"`
	Name     string             `json:"name,omitempty"`
	UserType userType           `json:"usertype,omitempty"`
}

func (userType userType) IsValid() error {
	switch userType {
	case Customer, Owner:
		return nil
	}
	return errors.New("invalid user type")
}
