package models

import (
	"api-server/app/db"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func AddDefaultUsers() {
	users := []User{
		{
			Username: "mecaf",
			Passwd:   "1234",
			Name:     "Mohammad",
			UserType: "owner",
		},
		{
			Username: "galiold",
			Passwd:   "5678",
			Name:     "Ali",
			UserType: "owner",
		},
		{
			Username: "customer1",
			Passwd:   "123",
			Name:     "Cust1",
			UserType: "customer",
		},
	}

	for _, user := range users {
		_, err := db.UserCollection.UpdateOne(
			context.TODO(),
			bson.M{"username": user.Username},
			bson.M{"$set": user},
			options.Update().SetUpsert(true),
		)
		if err != nil {
			log.Println(err)
		}
	}
}

func CreateRestaurantUniqueIndex() {
	if _, err := db.RestaurantCollection.Indexes().CreateOne(context.TODO(), mongo.IndexModel{
		Keys:    bson.M{"name": 1},
		Options: options.Index().SetUnique(true),
	}); err != nil {
		log.Fatal(err)
	}
}
