package models

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type orderStatus string

const (
	Queued   orderStatus = "queued"
	Pending  orderStatus = "pending"
	Received orderStatus = "received"
	Rejected orderStatus = "rejected"
)

type Order struct {
	User       primitive.ObjectID `bson:"user,omitempty"`
	Restaurant primitive.ObjectID `bson:"restaurant,omitempty"`
	Orders     []Food             `json:"user,omitempty"`
	Status     orderStatus        `json:"status,omitempty"`
}

type CreateOrd struct {
	Username string
	Foods []Food
}

type ChangeStatus struct {
	Action string
}

func (userType orderStatus) IsValid() error {
	switch userType {
	case Queued, Pending, Received:
		return nil
	}
	return errors.New("invalid order type")
}